<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control input-form',
                    'placeholder' => "Prénom"
                ]
            ])

            ->add('lastName', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control input-form',
                    'placeholder' => "Nom"
                ]
            ])

            ->add('telephone', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control input-form',
                    'placeholder' => "Téléphone"
                ]
            ])

            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control input-form',
                    'placeholder' => "Email"
                ]
            ])

            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Le mot de passe ne correspond pas.',
                'required' => true,
                'first_options'  => ['label' => false,
                                     'attr' => [
                                        'placeholder' => 'Mot de passe',
                                        'class' => 'form-control input-form']
                                    ],
                'second_options' => ['label' => false,
                                     'attr' => [
                                         'placeholder' => 'Confirmation',
                                         'class' => 'form-control input-form']
                                    ]
            ])

            ->add('username', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control input-form',
                    'placeholder' => "Pseudo"
                ]
            ])

            ->add('picture', FileType::class,[
                'label' => FALSE,
                'mapped' => false,
                'attr' => [
                    'class' => 'form-control-file',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
