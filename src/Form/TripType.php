<?php

namespace App\Form;

use App\Entity\Place;
use App\Entity\Town;
use App\Entity\Trip;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TripType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control input-form',
                    'placeholder' => "Nom"
                ]
            ])

            ->add('startDate', DateTimeType::class, [
                'label' => false,
                //'html5' => false,
                //'widget' => 'single_text',
                'attr' => [
                    
                ]
            ])

            ->add('duration', IntegerType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control input-form',
                    'placeholder' => "Durée"
                ]
            ])

            ->add('endDate', DateTimeType::class, [
                'label' => false,
                //'html5' => false,
                //'widget' => 'single_text',
                'attr' => [
                    
                ]
            ])

            ->add('nbMaxInscription', IntegerType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control input-form',
                    'placeholder' => "Nombre de places"
                ]
            ])

            ->add('description', TextareaType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'form-control textarea-form',
                    'placeholder' => "Description"
                ]
            ])

            ->add('place', EntityType::class, [
                'label' => FALSE,
                'attr' => [
                    'class' => 'form-control form-control-sm',
                ],
                'class' => Place::class,
                'choice_label' => function ($place) {
                    return $place->getName();
                }
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Trip::class,
        ]);
    }
}
