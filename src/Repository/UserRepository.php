<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findProfile($id)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT *
        FROM user U
        INNER JOIN campus C
        ON U.campus_id = C.id
        WHERE U.id = :id';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $id]);

        return $stmt->fetchAll();
    }

    public function findLastUser()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT id
        FROM user 
        ORDER BY id DESC
        LIMIT 1';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

}
