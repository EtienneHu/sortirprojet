<?php

namespace App\Repository;

use App\Entity\Trip;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Trip|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trip|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trip[]    findAll()
 * @method Trip[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TripRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trip::class);
    }


    public function findTrips($id)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM (
            SELECT T.id as id, T.name, T.start_date, T.end_date, COUNT(I.user_id) as nbInscrits, T.nb_max_inscription, T.avancement_id, A.wording as etat, U.first_name, U.last_name, T.leader_id FROM trip T
                    INNER JOIN avancement A
                    ON T.avancement_id = A.id
                    LEFT JOIN inscription I
                    ON T.id = I.trip_id
                    INNER JOIN place P
                    ON T.place_id = P.id
                    INNER JOIN user U
                    ON T.leader_id = U.id
                    GROUP BY T.id ) A
            LEFT JOIN (
                SELECT T.id AS id_tripB
                FROM trip T INNER JOIN inscription I ON T.id = I.trip_id WHERE I.user_id = :id) B
            ON A.id = B.id_tripB
                    
                    ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $id]);

        return $stmt->fetchAll();
    }

    public function findTrip($id)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT T.id, T.name, T.duration, T.description, T.start_date, T.end_date, COUNT(I.user_id) as nbInscrits, T.nb_max_inscription, T2.postal_code, P.name as placeName, P.street, P.latitude, P.longitude, U.first_name, U.last_name, T.leader_id 
        FROM trip T
        INNER JOIN avancement A
        ON T.avancement_id = A.id
        LEFT JOIN inscription I
        ON T.id = I.trip_id
        INNER JOIN place P
        ON T.place_id = P.id
        INNER JOIN user U
        ON T.leader_id = U.id
        INNER JOIN town T2
        ON P.town_id = T2.id
        WHERE T.id = :id';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $id]);

        return $stmt->fetchAll();
    }

    public function findParticipants($id)
    {

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT first_name, last_name, username 
        FROM user U
        INNER JOIN inscription I
        ON U.id = I.user_id
        INNER JOIN trip T
        ON I.trip_id = T.id
        WHERE T.id = :id';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $id]);

        return $stmt->fetchAll();
    }

    public function tripInscription($user_id, $trip_id)
    {

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'INSERT INTO inscription (user_id, trip_id) VALUES (:userId, :tripId)';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['userId' => $user_id,
                        'tripId' => $trip_id]);
    }

    public function tripUnInscription($user_id, $trip_id)
    {

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'DELETE FROM inscription WHERE user_id = :userId AND trip_id = :tripId';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['userId' => $user_id,
                        'tripId' => $trip_id]);
    }
}
