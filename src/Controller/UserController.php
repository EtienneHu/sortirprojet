<?php

namespace App\Controller;

use App\Entity\Campus;
use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class UserController extends AbstractController
{  

    /**
     * @Route("/", name="login")
     */
    public function login(){
        return $this->render("user/login.html.twig");
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(){

    }

    /**
     * @Route("/profile/accountModification", name="user_account_modification")
     */
    public function accountModification(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, UserInterface $user, SluggerInterface $slugger){
        
        $registerForm = $this->createForm(RegisterType::class, $user);

        $registerForm->handleRequest($request);

        if ($registerForm->isSubmitted() && $registerForm->isValid()){

            /** @var UploadedFile $pictureFile */
            $pictureFile = $registerForm->get('picture')->getData();

            if ($pictureFile) {
                $originalFilename = pathinfo($pictureFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$pictureFile->guessExtension();
                
                // Move the file to the directory where img are stored
                try {
                    $pictureFile->move(
                        $this->getParameter('pictures_directory'),
                        $newFilename
                        );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                
                $user->setPicture($newFilename);
            }

            //hash mdp
            $hashed = $encoder->encodePassword( $user, $user->getPassword());
            $user->setPassword($hashed);

            $em->persist($user);
            $em->flush();

        }

        return $this->render("user/accountModification.html.twig", [
            "registerForm" => $registerForm->createView()
        ]);
    }

     /**
     * @Route("/profile/userProfile/{id}", name="user_profile", requirements={"id": "\d+"})
     */
    public function userProfile(int $id){
        
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepo->findProfile($id);

        dump($user);

        return $this->render('user/profile.html.twig', [
            'user' => $user,
        ]);
    }
}
