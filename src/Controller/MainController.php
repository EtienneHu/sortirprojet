<?php

namespace App\Controller;

use App\Entity\Campus;
use App\Entity\Trip;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/profile/", name="home")
     */
    public function index()
    {

        $campusRepo = $this->getDoctrine()->getRepository(Campus::class);
        $campuss = $campusRepo->findAll();

        $id = $this->getUser()->getId();
        $active = $this->getUser()->getActive();

        $tripRepo = $this->getDoctrine()->getRepository(Trip::class);
        $trips = $tripRepo->findTrips($id);

        if ($active == false) {
            return $this->redirectToRoute('logout');
        }

        return $this->render('main/home.html.twig', [
            'controller_name' => 'MainController',
            'campuss' => $campuss,
            'trips' => $trips,
        ]);
    }
}
