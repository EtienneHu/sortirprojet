<?php

namespace App\Controller;

use App\Entity\Campus;
use App\Entity\Town;
use App\Entity\User;
use App\Form\CampusType;
use App\Form\RegisterType;
use App\Form\TownType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/towns", name="admin_towns")
     */
    public function villes( Request $request, EntityManagerInterface $em)
    {
        $town = new Town();

        $townForm = $this->createForm(TownType::class, $town);

        $townForm->handleRequest($request);

        if ($townForm->isSubmitted() && $townForm->isValid()){

            $em->persist($town);
            $em->flush();

            return $this->redirectToRoute('admin_towns');
        }

        $townsRepo = $this->getDoctrine()->getRepository(Town::class);
        $towns = $townsRepo->findAll();

        return $this->render('admin/towns.html.twig', [
            'controller_name' => 'AdminController',
            'towns' => $towns,
            'townForm' => $townForm->createView()
        ]);
    }

    /**
     * @Route("/towns/delete/{id}", name="towns_delete", requirements={"id": "\d+"})
     */
    public function deleteTown($id, EntityManagerInterface $em){
        
        $townsRepo = $this->getDoctrine()->getRepository(Town::class);
        $town = $townsRepo->find($id);
        
        $em->remove($town);
        $em->flush();
        
        return $this->redirectToRoute('admin_towns');
    }

    /**
     * @Route("/campus", name="admin_campus")
     */
    public function campus(Request $request, EntityManagerInterface $em)
    {
        $campus = new Campus();
        $campusForm = $this->createForm(CampusType::class, $campus);

        $campusForm->handleRequest($request);

        if ($campusForm->isSubmitted() && $campusForm->isValid()){

            $em->persist($campus);
            $em->flush();

            return $this->redirectToRoute('admin_campus');
        }

        $campusRepo = $this->getDoctrine()->getRepository(Campus::class);
        $campuss = $campusRepo->findAll();

        return $this->render('admin/campus.html.twig', [
            'controller_name' => 'AdminController',
            'campuss' => $campuss,
            'campusForm' => $campusForm->createView()
        ]);
    }

    /**
     * @Route("/campus/delete/{id}", name="campus_delete", requirements={"id": "\d+"})
     */
    public function deleteCampus($id, EntityManagerInterface $em){
        
        $campusRepo = $this->getDoctrine()->getRepository(Campus::class);
        $campus = $campusRepo->find($id);
        
        $em->remove($campus);
        $em->flush();
        
        return $this->redirectToRoute('admin_campus');
    }

    /**
     * @Route("/users", name="admin_users")
     */
    public function utilisateurs(Request $request, EntityManagerInterface $em)
    {
        $usersRepo = $this->getDoctrine()->getRepository(User::class);
        $users = $usersRepo->findAll();

        return $this->render('admin/users.html.twig', [
            'controller_name' => 'AdminController',
            'users' => $users, 
        ]);
    }

    /**
     * @Route("/createUser", name="admin_create_user")
     */
    public function createUser(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();

        $usersRepo = $this->getDoctrine()->getRepository(User::class);
        $lastUser = $usersRepo->findOneBy([], ['id' => 'desc']);
        
        $lastId = $lastUser->getId();

        $user->setFirstName("UserFirstName");
        $user->setLastName("UserLastName");
        $user->setEmail("User".($lastId+1)."Email@campus.fr");
        $hashed = $encoder->encodePassword( $user, "P@ssw0rd");
        $user->setPassword($hashed);
        $user->setRoles("ROLE_USER");
        $user->setActive(1);
        $user->setUsername("User".($lastId+1));

        $em->persist($user);
        $em->flush();
        
        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/users/delete/{id}", name="users_delete", requirements={"id": "\d+"})
     */
    public function deleteUsers($id, EntityManagerInterface $em){
        
        $usersRepo = $this->getDoctrine()->getRepository(User::class);
        $user = $usersRepo->find($id);
        
        $em->remove($user);
        $em->flush();
        
        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/users/desactive/{id}", name="users_desactive", requirements={"id": "\d+"})
     */
    public function desactiveUsers($id, EntityManagerInterface $em){
        
        $usersRepo = $this->getDoctrine()->getRepository(User::class);
        $user = $usersRepo->find($id);
        
        $user->setActive(false);
        $em->flush();
        
        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/users/active/{id}", name="users_active", requirements={"id": "\d+"})
     */
    public function activeUsers($id, EntityManagerInterface $em){
        
        $usersRepo = $this->getDoctrine()->getRepository(User::class);
        $user = $usersRepo->find($id);
        
        $user->setActive(true);
        $em->flush();
        
        return $this->redirectToRoute('admin_users');
    }
}
