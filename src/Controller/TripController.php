<?php

namespace App\Controller;

use App\Entity\Avancement;
use App\Entity\Trip;
use App\Entity\User;
use App\Form\TripType;
use App\Form\MotifType;
use App\Repository\InscriptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TripController extends AbstractController
{
    /**
     * @Route("/profile/createTrip/{id}", name="create_trip", requirements={"id": "\d+"})
     */
    public function CreateTrip( Request $request, EntityManagerInterface $em, $id)
    {
        $trip = new Trip();

        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $leader = $userRepo->find($id);

        $avancementRepo = $this->getDoctrine()->getRepository(Avancement::class);
        $avancement = $avancementRepo->find(1);

        $trip->setLeader($leader);
        $trip->setAvancement($avancement);

        $tripForm = $this->createForm(TripType::class, $trip);

        $tripForm->handleRequest($request);

        if ($tripForm->isSubmitted() && $tripForm->isValid()){

            $em->persist($trip);
            $em->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('trip/create_trip.html.twig', [
            'controller_name' => 'AdminController',
            'tripForm' => $tripForm->createView()
        ]);
    }

     /**
     * @Route("/profile/tripFullDisplay/{id}", name="trip_full_display", requirements={"id": "\d+"})
     */
    public function TripFullDisplay($id)
    {
        $tripRepo = $this->getDoctrine()->getRepository(Trip::class);
        $trip = $tripRepo->findTrip($id);

        $participantsRepo = $this->getDoctrine()->getRepository(Trip::class);
        $participants = $participantsRepo->findParticipants($id);

        return $this->render('trip/trip_full_display.html.twig', [
            'controller_name' => 'AdminController',
            'trip' => $trip,
            'participants' => $participants,
        ]);
    }

    /**
     * @Route("/profile/tripPublish/{id}", name="trip_publish", requirements={"id": "\d+"})
     */
    public function TripPublish(EntityManagerInterface $em, $id)
    {
        $tripRepo = $this->getDoctrine()->getRepository(Trip::class);
        $trip = $tripRepo->find($id);

        $avancementRepo = $this->getDoctrine()->getRepository(Avancement::class);
        $avancement = $avancementRepo->find(2);

        $trip->setAvancement($avancement);
        $em->flush();

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/profile/tripCancel/{id}", name="trip_cancel", requirements={"id": "\d+"})
     */
    public function TripCancel(Request $request, EntityManagerInterface $em, $id)
    {
        $tripRepo = $this->getDoctrine()->getRepository(Trip::class);
        $trip = $tripRepo->findTrip($id);
        $tripX = $tripRepo->find($id);

        $avancementRepo = $this->getDoctrine()->getRepository(Avancement::class);
        $avancement = $avancementRepo->find(6);

        $tripForm = $this->createForm(MotifTYpe::class, $tripX);

        $tripForm->handleRequest($request);

        if ($tripForm->isSubmitted() && $tripForm->isValid()){

            $tripX->setAvancement($avancement);

            $em->persist($tripX);
            $em->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('trip/trip_cancel.html.twig', [
            'controller_name' => 'AdminController',
            'trip' => $trip,
            'tripForm' => $tripForm->createView()
        ]);
    }

    /**
     * @Route("/profile/tripInscription/{id}", name="trip_inscription", requirements={"id": "\d+"})
     */
    public function TripInscription(EntityManagerInterface $em, $id)
    {
        $trip_id = $id;

        $user_id = $this->getUser()->getId();

        $tripRepo = $this->getDoctrine()->getRepository(Trip::class);

        $tripRepo->tripInscription($user_id, $trip_id);

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/profile/tripUnInscription/{id}", name="trip_un_inscription", requirements={"id": "\d+"})
     */
    public function TripUnInscription(EntityManagerInterface $em, $id)
    {
        $trip_id = $id;

        $user_id = $this->getUser()->getId();

        $tripRepo = $this->getDoctrine()->getRepository(Trip::class);

        $tripRepo->tripUnInscription($user_id, $trip_id);

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/profile/modifyTrip/{id}", name="modify_trip", requirements={"id": "\d+"})
     */
    public function ModifyTrip( Request $request, EntityManagerInterface $em, $id)
    {
        $tripRepo = $this->getDoctrine()->getRepository(Trip::class);
        $trip = $tripRepo->find($id);

        $tripForm = $this->createForm(TripType::class, $trip);

        $tripForm->handleRequest($request);

        if ($tripForm->isSubmitted() && $tripForm->isValid()){

            $em->persist($trip);
            $em->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('trip/modify_trip.html.twig', [
            'controller_name' => 'AdminController',
            'tripForm' => $tripForm->createView()
        ]);
    }

}
